### About me

Hi! I’m Chris Micek, Product design manager for Manage. I’m originally from Minnesota, but have lived in Amsterdam with my wife and cat since 2014; and my new son since 2021!

I came into design by failing my way through the computer science program at university until I discovered web tech. In my early career, I was often the only web person and learned that I enjoyed the planning and design of a website just as much as development. Eventually gravitating towards roles that would let me drive the creative process from ideation through to execution. Now, after working in UX for 15 years I"ve found myself working for Gitlab!

### Working with me

- My timezone is CET. Generally 9 to 6 with a long lunch.
- I am a naturally disorganised person, so I live and die by my calendar and notes. Unfortunately, if I don’t write something down I’m not going to remember it.
- I like to think out loud to work through problems in face-to-face meetings. I’ve been told it can be difficult to follow so please ask me to slow down or repeat something.
- Slack is the best way to get a hold of me throughout the day.


### Values and principles

- Great experiences are created at the intersection of writing, research, and design. So I encourage this diverse set of perspectives to be involved and collaborating from the beginning of a project. 
- Thoughtful writing is the foundation for quality design work. I’m intensely bothered by lorem ipsum text and handing off a design for writers to fill-in with copy. 
- Every role working on a product shapes the customer experience. I believe that it's part of our job in UX to encourage a user-centered mindset in our cross-functional teams. One of the best ways I’ve found to do this is by inviting others to participate in our design process as collaborators, not just feedback-givers.
- Visual communication is a powerful tool that can be used to influence strategy. By making abstract ideas tangible designers can show others a better version of our products to strive towards.

### Personally

I have a 1-year-old son, so my life outside of work revolved around keeping him happy and occupied. In my few free hours of the day I like to play simulation-style video games (Kerbal Space Program, Factorio, Rimworld), watch trashy American reality TV (Below Deck), and read Sci-Fi & Fantasy (Wheel of Time, Zone of Thought, Seveneves) 
